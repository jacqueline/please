package main

import (
	"context"
	"fmt"
	"log"
	"strings"
	"sync"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/resources/armresources"
	flag "github.com/spf13/pflag"
)

var (
	subscriptionID     string
	location           string
	resourceGroupName  string
	resourceGroupNames []string
	cmd                string
)

func init() {
	flag.StringVarP(&subscriptionID, "subscription-id", "s", "", "Azure Subscription ID")
	flag.StringVarP(&location, "location", "l", "westeurope", "Azure region to create resource group in")
	flag.StringSliceVarP(&resourceGroupNames, "group-names", "g", []string{}, "Resource group name(s) to create/delete")
	flag.StringVarP(&cmd, "cmd", "c", "create", "Subcommand to run: create or delete")
	flag.Parse()
}

func main() {
	cred, err := azidentity.NewDefaultAzureCredential(nil)
	if err != nil {
		log.Fatalf("Authentication failure: %+v", err)
	}

	switch cmd {
	case "create":
		names := resourceGroupNames
		responses, err := createResourceGroups(subscriptionID, cred, location, names)
		if err != nil {
			log.Fatalf("Failed to create resource groups: %v", err)
		}

		for _, resp := range responses {
			log.Printf("Created resource group %s in location %s", *resp.ResourceGroup.ID, *resp.ResourceGroup.Location)
		}
	case "delete":
		err := deleteResourceGroups(subscriptionID, cred, resourceGroupNames)
		if err != nil {
			log.Fatalf("Deletion of resource group failed: %+v", err)
		}
		log.Printf("Resource group %s deleted", resourceGroupName)
	default:
		log.Fatalf("Unknown subcommand: %s", cmd)
	}
}

func createResourceGroups(subscriptionID string, credential azcore.TokenCredential, location string, names []string) ([]armresources.ResourceGroupsClientCreateOrUpdateResponse, error) {
	rgClient, err := armresources.NewResourceGroupsClient(subscriptionID, credential, nil)
	if err != nil {
		return nil, err
	}

	var responses []armresources.ResourceGroupsClientCreateOrUpdateResponse

	for _, name := range names {
		param := armresources.ResourceGroup{
			Location: &location,
		}

		resp, err := rgClient.CreateOrUpdate(context.Background(), name, param, nil)
		if err != nil {
			return nil, fmt.Errorf("failed to create resource group %s: %v", name, err)
		}

		responses = append(responses, resp)
	}

	return responses, nil
}

// Define the function to delete multiple resource groups.
func deleteResourceGroups(subscriptionID string, credential azcore.TokenCredential, resourceGroups []string) error {
	var wg sync.WaitGroup
	var errList []error

	for _, rgName := range resourceGroups {
		log.Printf("Deleting resource group %s", rgName)
		wg.Add(1)

		go func(name string) {
			defer wg.Done()

			rgClient, err := armresources.NewResourceGroupsClient(subscriptionID, credential, nil)
			if err != nil {
				errList = append(errList, fmt.Errorf("failed to create resource group client: %v", err))
				return
			}

			poller, err := rgClient.BeginDelete(context.Background(), name, nil)
			if err != nil {
				errList = append(errList, fmt.Errorf("failed to begin delete resource group %s: %v", name, err))
				return
			}

			if _, err := poller.PollUntilDone(context.Background(), nil); err != nil {
				errList = append(errList, fmt.Errorf("failed to delete resource group %s: %v", name, err))
				return
			}
			log.Printf("Resource group %s deleted", name)
		}(rgName)
	}

	wg.Wait()
	if len(errList) > 0 {
		errStrs := make([]string, len(errList))
		for i, err := range errList {
			errStrs[i] = err.Error()
		}
		return fmt.Errorf("%s", strings.Join(errStrs, "\n"))
	}

	return nil
}
