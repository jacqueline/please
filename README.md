# please

Hobby project to make a command line tool for Azure with commands I often use.


```sh
mkdir -p please/cmd
touch cmd/main.go

# go mod init codeberg.org/jacqueline/please

go get -d ./...
go get -u ./...

```
Run like so

```sh
/main -s <subscription-id> -g toys-rg,rg-petclinic-f8ba5,fud01-kube-rg -c delete
```



Example Dockerfile

```dockerfile
# Build stage
FROM golang:1.20 AS builder

WORKDIR /app

# Copy the source code
COPY . .

# Build the Go binary
RUN CGO_ENABLED=0 GOOS=linux go build -o myapp .

# Final stage
FROM alpine:latest

WORKDIR /app

# Copy the binary from the build stage
COPY --from=builder /app/myapp .

# Run the binary
CMD ["./myapp"]
```